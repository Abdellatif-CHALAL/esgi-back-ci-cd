<?php

namespace tests\Controller;

use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends WebTestCase
{

    public function testNewTodoNominal()
    {


        $data = array
        (
            "title"  => "todo item 1"
        );
 
        
        $this->postData($client, $data, '/new');
        $response = $client->getResponse();
        $this->assertStatusCodeResponse($response, 201);

        $title = "todo item 1";

        $client = static::createClient();
        $client->request('POST', '/new', {
            "title" => $title
        }
        );

        $this->assertResponseStatusCodeSame(201);
    }
}
