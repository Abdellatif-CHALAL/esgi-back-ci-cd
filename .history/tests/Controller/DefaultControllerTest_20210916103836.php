<?php

namespace tests\Controller;

use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends WebTestCase
{

    public function testNewTodoNominal()
    {
        $title = "todo item 1";

        $client = static::createClient();
        $client->getRequest(
            'POST',
            '/new',
            [
                "title" => $title
            ]
        );

        $this->assertResponseStatusCodeSame(201);
    }
}
