<?php

namespace tests\Controller;

use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends WebTestCase
{

    public function testNewTodoNominal()
    {


        $data = array
        (
            "body"  => "This is a comment",
            "status"  => "1",
            "user_id" => "1",
            "movie_id"  => "4",
        );
 
        $client = $this->createAuthenticatedClient('api@api.com', 'api');
        $this->postData($client, $data, '/api/comments');
        $response = $client->getResponse();
        $this->assertStatusCodeResponse($response, 201);
        
        $title = "todo item 1";

        $client = static::createClient();
        $client->request('POST', '/new', {
            "title" => $title
        }
        );

        $this->assertResponseStatusCodeSame(201);
    }
}
