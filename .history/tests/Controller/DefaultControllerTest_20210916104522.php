<?php

namespace tests\Controller;

use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends WebTestCase
{

    public function testIndexTodo()
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful(200);
    }


    public function testNewTodo()
    {
        $client = static::createClient();
        $client->request('POST', '/new', [
            "title" => "title for test"
        ]);

        $this->assertResponseIsSuccessful(200);
    }
}
