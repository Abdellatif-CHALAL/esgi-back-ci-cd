<?php

namespace tests\Controller;

use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends WebTestCase
{

    public function testIndexTodo()
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful(200);
    }


    public function testNewTodo()
    {


        $client = new \GuzzleHttp\Client([
            'base_url' => 'http://localhost:8000',
            'defaults' => [
                'exceptions' => false
            ]
        ]);
        $data = array(
            "title" => "title for test"
        );
        // 1) Create a programmer resource
        $response = $client->post('/new', [
            'body' => json_encode($data)
        ]);

        $client = static::createClient();
        $client->request(
            'POST',
            '/new',
            [
                "title" => "title for test"
            ],
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );

        $this->assertResponseIsSuccessful(201);
    }
}
