<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Todo;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index", methods="GET")
     */
    public function index(TodoRepository $todoRepository, NormalizerInterface $serialize)
    {
        $todos = $todoRepository->findAll();
        $json = $serialize->serialize($todos, 'json', []);

        return new Response($json, 200, ['Access-Control-Allow-Origin' => '*', "Content-Type" => "application/json"]);
    }

    /**
     * @Route("/new", name="new", methods="POST")
     */
    public function new(Request $request, EntityManagerInterface $em, NormalizerInterface $serialize)
    {
        // $data = json_decode($request->getContent());
        $title = $data->title;

        $todo = new Todo();
        $todo->setTitle($title);
        $todo->setCompleted(false);

        $em->persist($todo);
        $em->flush();

        $json = $serialize->serialize($todo, 'json', []);

        return new Response($json, 201, ['Access-Control-Allow-Origin' => '*', "Content-Type" => "application/json"]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     */
    public function show($id, TodoRepository $todoRepository, NormalizerInterface $serialize)
    {
        $todo = $todoRepository->find($id);
        $json = $serialize->serialize($todo, 'json', []);
        if (!$todo) {
            return new Response("todo list doesn't exist", 400);
        }
        return new Response($json, 200, ['Access-Control-Allow-Origin' => '*', "Content-Type" => "application/json"]);
    }
}
