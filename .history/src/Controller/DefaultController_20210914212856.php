<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TodoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController
{
    public function index(TodoRepository $todoRepository)
    {

        return $this->json_decode($todoRepository->findAll(), 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
