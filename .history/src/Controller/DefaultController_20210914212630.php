<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TodoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController
{
    public function index(TodoRepository $todoRepository,Normalizer): JsonResponse
    {
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers);
        return new JsonResponse($todoRepository->findAll(), 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
