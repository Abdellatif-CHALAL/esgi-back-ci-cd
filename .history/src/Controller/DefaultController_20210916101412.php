<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Todo;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index", methods="GET")
     */
    public function index(TodoRepository $todoRepository, NormalizerInterface $serialize)
    {
        $todos = $todoRepository->findAll();
        $json = $serialize->serialize($todos, 'json', []);

        return new Response($json, 200, ['Access-Control-Allow-Origin' => '*', "Content-Type" => "application/json"]);
    }

    /**
     * @Route("/new", name="new", methods="POST")
     */
    public function new(Request $request, EntityManagerInterface $em, NormalizerInterface $serialize)
    {
        $data = json_decode($request->getContent());
        $title = $data->title;

        $todo = new Todo();
        $todo->setTitle($title);
        $todo->setCompleted(false);

        $em->persist($todo);
        $em->flush();

        $json = $serialize->serialize($todo, 'json', []);

        return new Response($json, 201, ['Access-Control-Allow-Origin' => '*', "Content-Type" => "application/json"]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     */
    public function show($id, TodolistRepository $TodolistRepository, NormalizerInterface $serialize): JsonResponse
    {
        $Todolist = $TodolistRepository->find($id);
        if (!$Todolist) {
            return new JsonResponse("todo list doesn't exist", 400);
        }
        return new JsonResponse(["todo list" => $Todolist], 200);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"PUT"})
     */
    public function edit(Request $request, $id, TodoRepository $todoRepository, NormalizerInterface $serialize, EntityManagerInterface $em): JsonResponse
    {
        $Todo = $todoRepository->find($id);
        if (is_null($Todo)) {
            return new JsonResponse(["error" => "the todo list doesn't exist"], 400);
        }
        $data = $request->getContent();
        try {
            $TodoEdited = $serializer->deserialize($data, Todo::class, 'json');
            $Todo->setTitle($TodoEdited->getTitle());
            $Todo->setCompleted($TodoEdited->getCompleted());

            $em->persist($Todo);
            $em->flush();

            return new JsonResponse($Todo, 200);
        } catch (NotEncodableValueException $e) {
            return new JsonResponse(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"DELETE"})
     */
    public function delete($id, TodolistRepository $TodolistRepository, EntityManagerInterface $em): JsonResponse
    {
        $Todolist = $TodolistRepository->find($id);
        if (!$Todolist) {
            return new JsonResponse(["error" => "the todo list doesn't exist"], 400);
        }

        $em->remove($Todolist);
        $em->flush();

        return new JsonResponse("deleted with success", 200);
    }
}
