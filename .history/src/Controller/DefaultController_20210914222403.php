<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(TodoRepository $todoRepository, NormalizerInterface $serialize): JsonResponse
    {
        $todos = $todoRepository->findAll();
        $json = $serialize->serialize($todos, 'json', []);

        return new Response($json, 200, ['Access-Control-Allow-Origin' => '*', "Content-Type" => "application/json"]);
    }
}
