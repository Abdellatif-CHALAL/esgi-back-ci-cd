<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(TodoRepository $todoRepository): JsonResponse
    {
        return new JsonResponse(json_decode($todoRepository->findAll()), 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
