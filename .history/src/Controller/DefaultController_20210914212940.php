<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    public function index(TodoRepository $todoRepository)
    {

        return $this->js($todoRepository->findAll(), 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
