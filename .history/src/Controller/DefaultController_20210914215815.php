<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TodoRepository;
use Normalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Polyfill\Intl\Normalizer\Normalizer as NormalizerNormalizer;

class DefaultController extends AbstractController
{
    public function index(TodoRepository $todoRepository): JsonResponse
    {

        $json = json_encode($todoRepository->findAll());
        return $this->json($json, 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
