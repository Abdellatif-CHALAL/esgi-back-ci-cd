<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use App\Repository\TodoRepository;
use Normalizer;
use PhpParser\Node\Expr\Cast\Array_;
use Serializable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    public function index(TodoRepository $todoRepository, NormalizerInterface $serialize): JsonResponse
    {
        $todos = $todoRepository->findAll();
        $json = $serialize->serialize($todos, 'json', []);

        return new JsonResponse($json, 200, ['Access-Control-Allow-Origin' => '*', "Content-Type" => "application/json"]);
    }
}
