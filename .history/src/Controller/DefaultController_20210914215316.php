<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TodoRepository;
use Normalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Polyfill\Intl\Normalizer\Normalizer as NormalizerNormalizer;

class DefaultController extends AbstractController
{
    public function index(TodoRepository $todoRepository, Normalizer $normalizer): JsonResponse
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $productSerialized = $serializer->serialize($product, 'json');

        return new JsonResponse(json_decode($todoRepository->findAll()), 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
